var closeRange_basicDamageArray = {
	"---" : 0,
	"D.Va" : "10,000",
	"Lucio" : "5,000",
	"Reaper" : "10,000",
	"Roadhog" : "10,000",
	"Sombra" : "5,000",
	"Symmetra" : "10,000",
	"Tracer" : "8,000",
	"Winston" : "5,000",
	"Wrecking Ball" : "8,000",
	"Zarya" : "10,000",
};
var closeRange_basicKillArray = {
	"---" : 0,
	"D.Va" : 15,
	"Lucio" : 10,
	"Reaper" : 15,
	"Roadhog" : 15,
	"Sombra" : 15,
	"Symmetra" : 15,
	"Tracer" : 15,
	"Winston" : 15,
	"Wrecking Ball" : 15,
	"Zarya" : 15,
};
function closeRange_changeValue(){
	var selectedHero = $('#closeRange_heroSelect');
	document.getElementById("closeRange_selectedHero").innerHTML = selectedHero.val();
	document.getElementById("closeRange_basicDamage").innerHTML = closeRange_basicDamageArray[selectedHero.val()];
	document.getElementById("closeRange_basicKills").innerHTML = closeRange_basicKillArray[selectedHero.val()];
}